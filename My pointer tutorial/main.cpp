/*
*
* Pointer tutorial
*
*/

#include <stdio.h>

int main()
{
	unsigned char value = 100;
	unsigned char *r = NULL;
	unsigned char **p = NULL;

	r = &value;
	p = &r;	//pointing to the pointer

//************* NULL POINTER section **************************
#ifdef NULL_PTR
//	delete(r);	//deleteing a pointer that was allocated of the stack will crash the program
//	delete(p);	//deleteing a double pointer that was allocated of the stack will crash the program
	int *s = new int;	//allocating memory from the heap.
	*s = 10;
//	delete(s);	//deleting the memory thought the pointer points the pointer somewhere unknown.
				//It's advisable to set s to NULL or nullptr.
//	delete(s);	//deleting a deleted pointer again will crash your program.
	int **w = NULL; 
	w = &s;		//pointing double pointer to s
//	delete(w);	//deleteing the couble pointer will crash your program
	delete(*w);	//deleting where the double pointer's pointer is OK. This will delete s in this example.
//	delete(*w);	//deleting where the double pointer's pointer is again will crash your program.
#endif //NULL_PTR
//**************************************************************

	printf("pointer r points to address of \"value\" --> r = &value\n");
	printf("double pointer p points to address of r --> p = &r\n");
	printf("\n");
	printf("contents of r --> (*r) is: 0x%x = %d\n", *r, *r);
	printf("address of r --> (&r) is: 0x%x\n", &r);
	printf("\n");
	printf("contents of the contents of p --> (**p) is: 0x%x\n", **p);
	printf("address of the contents of p --> (&*p) is: 0x%x\n", &*p);
	printf("contents of the address of p --> (*&p) is: 0x%x\n", *&p);
	printf("can't do &&p\n");
	printf("\n");
	printf("contents of the address of r --> (*&r) is: 0x%x\n", *&r);
	printf("address of the contents of r --> (&*r) is: 0x%x\n", &*r);
	printf("can't do **r or &&r\n");
	printf("\n");
	printf("address of the contents of the contents of p --> (&**p) is: 0x%x\n", &**p);
	printf("contents of the contents of the address of p --> (**&p) is: 0x%x\n", **&p);
	printf("contents of the address of the contents of p --> (*&*p) is: 0x%x\n", *&*p);
	printf("address of the contents of the address of p --> (&*&p) is: 0x%x\n", &*&p);
	printf("can't do ***p or *&*p or *&&p or &&&p or &&*\n");
	printf("\n");
	printf("contents of the contents of the address of r --> (**&r) is: 0x%x\n", **&r);
	printf("contents of the address of the contents of r --> (*&*r) is: 0x%x\n", *&*r);
	printf("address of the contents of the address of r --> (&*&r) is: 0x%x\n", &*&r);
	printf("can't do ***r or &&&r or &&*r or *&&r or &**r\n");
	printf("\n");
	printf("typecast double pointer of the address of r --> (void **)&r is: 0x%x\n", (void **)&r);
	printf("typecast double pointer of the contents of r --> (void **)*r is: 0x%x\n", (void **)*r);
}